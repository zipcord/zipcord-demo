import { makeConfig } from './rollup.common'


export default makeConfig('src/bench.ts',
  ['src/bench.html', 'node_modules/zipcord-decoder-flate2/decoder-pkg'],
)
