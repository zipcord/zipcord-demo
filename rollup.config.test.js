import { sizeSnapshot } from 'rollup-plugin-size-snapshot'
import visualizer from 'rollup-plugin-visualizer'

import { makeConfig } from './rollup.common'


export default makeConfig(
  'src/test.ts',
  ['src/test.html', 'node_modules/zipcord-decoder-flate2/decoder-pkg'],
  [
    sizeSnapshot(),
    visualizer({
      title: 'Visualization of test.ts',
      brotliSize: true,
      gzipSize: true,
    })
  ]
)
