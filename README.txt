Demo site for Zipcord. Provides examples of how to use it, and to
test/benchmark its functionality.

Demo: https://zipcord.gitlab.io/zipcord-demo/
Benchmark: https://zipcord.gitlab.io/zipcord-demo/bench.html
Browser test: https://zipcord.gitlab.io/zipcord-demo/test.html
