// TODO:NPM: Reenable flate2 decoder when published to NPM

import { ZipEntry, ZipcordRaw } from 'zipcord'
import { fflateDecoderSetup } from 'zipcord-decoder-fflate'
// import { createFlate2Setup } from 'zipcord-decoder-flate2'
import { pakoDecoderSetup } from 'zipcord-decoder-pako'

import { getElementOrError } from './util'


type BenchmarkResults = Map<string, number>

const ZIP_ENTRY_NAME = 'camels-uncompressed.png'
const ZIP_ENTRY_SIZE = 1401531
const ZIP_URL = 'example.zip'

const ITERATION_COUNT_ELEMENT = getElementOrError<HTMLInputElement>('numIterations')
const OUTPUT_ELEMENT = getElementOrError('output')
const START_BUTTON_ELEMENT = getElementOrError<HTMLButtonElement>('startButton')

function renderResults(times: BenchmarkResults, iterations: number): void {
  while (OUTPUT_ELEMENT.lastChild) {
    OUTPUT_ELEMENT.lastChild.remove()
  }

  const longest = Math.max(...Array.from(times.values()))
  let totalTime = 0
  for (const [label, time] of times) {
    totalTime += time
    const resultEntry = document.createElement('div')
    resultEntry.className = 'resultEntry'
    resultEntry.style.position = 'relative'
    const timeSeconds = time / 1000
    /** Average rate in MB/s */
    const speed = (ZIP_ENTRY_SIZE / (timeSeconds / iterations)) / 1024**2
    const timeDetails = `${timeSeconds.toFixed(2)}s (${speed.toFixed(2)} MB/s)`
    resultEntry.innerHTML = `
      <div class="resultBar" style="width: ${(time / longest) * 100}%">&nbsp;</div>
      <div>${label}: ${timeDetails}</div>
    `
    OUTPUT_ELEMENT.append(resultEntry)
  }

  const outputNotes = document.createElement('div')
  outputNotes.innerText = `Smaller times are better. Total time to run benchmark: ${(totalTime / 1000).toFixed(2)}s`
  OUTPUT_ELEMENT.append(outputNotes)
}

async function timeExtract(entry: ZipEntry, iterations: number): Promise<number> {
  const start = performance.now()
  for (let i = 0; i < iterations; i++) {
    await entry.buffer()
  }
  const end = performance.now()

  return end - start
}

async function runBenchmark(archives: Map<string, ZipcordRaw>, iterations: number): Promise<BenchmarkResults> {
  const results = new Map()
  const entries = Array.from(archives.entries())
  for (const [index, [archiveName, archive]] of entries.entries()) {
    OUTPUT_ELEMENT.innerText = `Running benchmark ${index+1}/${archives.size} (${archiveName})...`
    const entry = archive.entries.get(ZIP_ENTRY_NAME)
    if (!entry) {
      throw new Error('Archive entry not found')
    }
    results.set(archiveName, await timeExtract(entry, iterations))
  }
  return results
}

async function loadData(): Promise<Map<string, ZipcordRaw>> {
  const archiveResponse = await fetch(ZIP_URL)
  const archiveFile = await archiveResponse.blob()
  const zipFflate = await ZipcordRaw.open(archiveFile, {
    decoderSetup: fflateDecoderSetup
  })
  const zipFflateNoStreams = await ZipcordRaw.open(archiveFile, {
    decoderSetup: fflateDecoderSetup, disableInternalStreams: true
  })
  // const zipFlate2 = await ZipcordRaw.open(archiveFile, {
  //   decoderSetup: createFlate2Setup('decoder-pkg/index_bg.wasm')
  // })
  // const zipFlate2NoStreams = await ZipcordRaw.open(archiveFile, {
  //   decoderSetup: createFlate2Setup('decoder-pkg/index_bg.wasm'),
  //   disableInternalStreams: true
  // })
  // await zipFlate2.decoder
  const zipPako = await ZipcordRaw.open(archiveFile, {
    decoderSetup: pakoDecoderSetup
  })
  const zipPakoNoStreams = await ZipcordRaw.open(archiveFile, {
    decoderSetup: pakoDecoderSetup, disableInternalStreams: true
  })

  const archives = new Map([
    ['fflate', zipFflate],
    ['fflate (no streams)', zipFflateNoStreams],
    // ['Flate2', zipFlate2],
    // ['Flate2 (no streams)', zipFlate2NoStreams],
    ['Pako', zipPako],
    ['Pako (no streams)', zipPakoNoStreams]
  ])

  return archives
}

async function main(): Promise<void> {
  const archives = await loadData()
  OUTPUT_ELEMENT.innerText = 'Ready to run benchmark'

  START_BUTTON_ELEMENT.onclick = async () => {
    OUTPUT_ELEMENT.innerText = 'Running benchmark...'
    START_BUTTON_ELEMENT.disabled = true
    const iterationCount = parseInt(ITERATION_COUNT_ELEMENT.value)
    const results = await runBenchmark(archives, iterationCount)
    renderResults(results, iterationCount)
    START_BUTTON_ELEMENT.disabled = false
  }
  START_BUTTON_ELEMENT.disabled = false
}
main()
