/** Common utility code for demo pages */


export function getElementOrError<E extends HTMLElement>(elementId: string): E {
  const element = document.getElementById(elementId)
  if (!element) {
    throw new Error(`Could not get element with ID ${elementId}`)
  }
  return element as E
}
