// TODO:NPM: Reenable flate2 decoder when published to NPM

import { Zipcord, ZipcordRaw, ZipEntry } from 'zipcord'
import { fflateDecoderSetup } from 'zipcord-decoder-fflate'
// import { createFlate2Setup } from 'zipcord-decoder-flate2'
import { pakoDecoderSetup } from 'zipcord-decoder-pako'

import type { ZipcordOptions } from 'zipcord'

import { getElementOrError } from './util'


const ZIP_COMMENT_HASH = 2517522006
const ZIP_CREDITS_HASH = 1545640097
const ZIP_CREDITS_NAME = 'credits.txt'
const ZIP_CREDITS_TIME = 1598656764000
const ZIP_FILENAMES = [
  'bee.jpg',
  'camels-uncompressed.png',
  'clownfish.jpg',
  'credits.txt',
  'penguins.jpg',
  'pirate.txt',
]
const ZIP_PIRATE_HASH = 3971811344
const ZIP_PIRATE_NAME = 'pirate.txt'
const ZIP_URL = 'example.zip'

const OUTPUT_ELEMENT = getElementOrError('output')

/** Writes a line to the output */
function out(text: string): void {
  OUTPUT_ELEMENT.innerHTML += text + '\n'
}

const tester = {
  successful: 0,
  total: 0,

  /** Converts a boolean into a unicode check/cross (uc === Unicode Checkmark) */
  uc(value: boolean): string {
    return value ? '✓' : '✗'
  },

  /** Writes output along with results of a test */
  outtest(label: string, result: boolean): void {
    let formattedLabel = label
    if (result === false) {
      formattedLabel = `<span class="failed">${formattedLabel}</span>`
    }
    out(`${formattedLabel}: ${this.uc(result)}`)
  },

  async test(description: string, fn: () => Promise<boolean>) {
    try {
      const result = await fn()
      this.outtest(description, result)
      this.successful++
    } catch (e) {
      this.outtest(description, false)
      out('\t' + e)
    }
    this.total++
  }
}

/** Get an entry or throw an error */
function entryOrFail(archive: Zipcord, name: string): ZipEntry {
  const entry = archive.entries.get(name)
  if (!entry) {
    const message = `Archive entry ${name} not present`
    out(`<span class="failed">${message}`)
    throw new Error(message)
  }
  return entry
}

const encoder = new TextEncoder()
/** SHA-256 of a string */
async function hashString(str: string): Promise<number> {
  const strBuf = await crypto.subtle.digest('SHA-256', encoder.encode(str))
  // const cc = new DataView(c)
  return (new DataView(strBuf)).getUint32(0)
}

async function collectTextStream(stream: ReadableStream): Promise<string> {
  let text = ''
  const reader = stream.getReader()
  while (true) {
    const chunk: ReadableStreamReadResult<string> = await reader.read()
    if (chunk.value) {
      text += chunk.value
    }
    if (chunk.done) { break }
  }
  return text
}

async function testDecoder(name: string, buffer: ArrayBuffer, decoder: ZipcordOptions['decoderSetup']): Promise<void> {
  // Streams
  {
    const zipStreams = await ZipcordRaw.open(buffer, {
      checkCRC32: true,
      decoderSetup: decoder
    })
    const pirateEntryStreams = entryOrFail(zipStreams, ZIP_PIRATE_NAME)
    await tester.test(`Decompress using ${name} (buffer)`,
      async () => await hashString(await pirateEntryStreams.text()) === ZIP_PIRATE_HASH
    )
    await tester.test(`Decompress using ${name} (stream)`,
      async () => {
        const pirateStreamText = await collectTextStream(await pirateEntryStreams.textStream())
        return await hashString(pirateStreamText) === ZIP_PIRATE_HASH
      }
    )
  }
  // No streams
  {
    const zipNoStreams = await ZipcordRaw.open(buffer, {
      checkCRC32: true,
      decoderSetup: decoder,
      disableInternalStreams: true
    })
    const pirateEntryNoStreams = entryOrFail(zipNoStreams, ZIP_PIRATE_NAME)
    await tester.test(`Decompress using ${name} (streams disabled)`,
      async () => await hashString(await pirateEntryNoStreams.text()) === ZIP_PIRATE_HASH
    )
  }
}

async function main(): Promise<void> {
  // Clear the output
  OUTPUT_ELEMENT.textContent = ''

  // Check for features
  await tester.test('TransformStream present',
    async () => globalThis.TransformStream !== undefined
  )
  await tester.test('Blob.stream() present',
    async () => globalThis.Blob?.prototype.stream !== undefined
  )

  out('\n')
  // Previous tests were just to show status of optional features, reset count
  tester.successful = 0
  tester.total = 0

  // Reading from URL
  {
    const zip = await Zipcord.open(ZIP_URL)
    await tester.test('Reading in URL mode',
      async () => zip.readingFromUrl === true
    )
    await tester.test('Archive comment parsed',
      async () => zip.comment !== undefined && await hashString(zip.comment) === ZIP_COMMENT_HASH
    )
    await tester.test('All file entries found',
      async () => JSON.stringify(zip.filenames.sort()) === JSON.stringify(ZIP_FILENAMES)
    )

    /** ZipEntry for uncompressed data */
    const creditsEntry = entryOrFail(zip, ZIP_CREDITS_NAME)
    /** ZipEntry for compressed data */
    const pirateEntry = entryOrFail(zip, ZIP_PIRATE_NAME)
    await tester.test('Date is properly parsed',
      async () => creditsEntry.date.getTime() === ZIP_CREDITS_TIME
    )
    await tester.test('Extract an uncompressed archive entry',
      async () => await hashString(await creditsEntry.text()) === ZIP_CREDITS_HASH
    )
    await tester.test('Extract a compressed archive entry',
      async () => await hashString(await pirateEntry.text()) === ZIP_PIRATE_HASH
    )

    await tester.test('Switch to buffer reading mode',
      async () => {
        await zip.disableUrlReading()
        return zip.readingFromUrl === false
      }
    )
  }

  const buffer = await (await fetch(ZIP_URL)).arrayBuffer()

  // Reading from buffers
  await tester.test('Open archive as an ArrayBuffer',
    async () => (await Zipcord.open(buffer)).filenames.length > 0
  )
  await tester.test('Open archive as a Blob',
    async () => (await Zipcord.open(new Blob([buffer]))).filenames.length > 0
  )

  // CRC-32
  {
    const zipCRC32 = await Zipcord.open(buffer, {checkCRC32: true})
    const creditsEntryCRC32 = entryOrFail(zipCRC32, ZIP_PIRATE_NAME)
    await tester.test('Read entry checking CRC32',
      async () => (await creditsEntryCRC32.buffer()).length > 0
    )
  }

  // Streams
  {
    const zipNoStreams = await Zipcord.open(buffer, {disableInternalStreams: true})
    const creditsNoStreams = entryOrFail(zipNoStreams, ZIP_PIRATE_NAME)
    await tester.test('Read entry with streams disabled',
      async () => (await creditsNoStreams.buffer()).length > 0
    )
  }
  {
    const zipReadable = await Zipcord.open(buffer)
    const pirateReadable = entryOrFail(zipReadable, ZIP_PIRATE_NAME)
    await tester.test('Read entry as a ReadableStream',
      async () => {
        const streamText = await collectTextStream(await pirateReadable.textStream())
        const streamHash = await hashString(streamText)
        return streamHash === ZIP_PIRATE_HASH
      }
    )
  }

  await testDecoder('zipcord-decoder-fflate', buffer, fflateDecoderSetup)
  // await testDecoder('zipcord-decoder-flate2', buffer, createFlate2Setup('decoder-pkg/index_bg.wasm'))
  await testDecoder('zipcord-decoder-pako', buffer, pakoDecoderSetup)

  out(`\nAll tests completed (${tester.successful}/${tester.total} successful)`)
}
main()
