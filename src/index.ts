import { ZipEntry, Zipcord } from 'zipcord'

import { getElementOrError } from './util'


// Add Zipcord to global window object to allow access from console
declare global {
  interface Window {
    Zipcord: typeof Zipcord
  }
}
window.Zipcord = Zipcord

/** Image formates supported by modern browsers */
const IMAGE_EXTENSIONS = ['bmp', 'gif', 'ico', 'jpeg', 'jpg', 'png', 'webp']
/** URL to revoke with revokeObjectURL when the viewer closes */
let currentObjectUrl = ''

// Preload all the elements used by the UI so we don't scatter getElementById()
// as HTMLWhateverElement calls all over the place.
// Technically getElementById could return null if the element doesn't exist
// but ignore that to keep the demo code simple.

/** HTML <a> element for downloading archive entries */
const downloadElement = getElementOrError<HTMLAnchorElement>('downloadElement')
/** HTML <input type="file"> element for opening local zip archives */
const fileBlobInputElement = getElementOrError<HTMLInputElement>('fileInput')
/** HTML <button> element for opening URLs */
const fileUrlButtonElement = getElementOrError<HTMLButtonElement>('fileUrlLoad')
/** HTML <input type="url"> element for setting URL source to open */
const fileUrlInputElement = getElementOrError<HTMLButtonElement>('fileUrl')
/** HTML <div> element containing archive comment if present */
const archiveCommentElement = getElementOrError<HTMLUListElement>('archiveComment')
/** HTML <ul> element containing archive contents listing */
const archiveListingElement = getElementOrError<HTMLUListElement>('archiveListing')
/** HTML <h2> element containing archive URL or filename */
const archiveNameElement = getElementOrError<HTMLHeadingElement>('archiveName')
/** HTML <div> element containing archive entry viewer */
const viewerElement = getElementOrError<HTMLDivElement>('viewer')
/** HTML <div> element containing entry comment in viewer */
const viewerCommentElement = getElementOrError<HTMLDivElement>('viewerComment')
/** HTML <h2> element containing entry filename in viewer */
const viewerNameElement = getElementOrError<HTMLHeadingElement>('viewerName')
/** HTML <div> element containing entry contents in viewer */
const viewerContentsElement = getElementOrError<HTMLDivElement>('viewerContents')


/** Returns file extension or empty string if no extension */
function findExt(path: string): string {
  // Regex matches everything after the final '.' or returns ['', undefined] if
  // no extension. Guaranteed to result in a 2-length array, can skip null check
  const ext = (path.match(/(?:\.([^.]+))?$/) as RegExpMatchArray)[1]
  return ext !== undefined ? ext : ''
}

/** Clear contents of a DOM element */
function clearElement(element: Element): void {
  while (element.lastChild) {
    element.lastChild.remove()
  }
}

/** Sets the archive contents to an error message */
function setErrorMessage(error: Error): void {
  clearElement(archiveListingElement)
  archiveNameElement.innerText = 'Error opening archive'
  archiveCommentElement.innerText = error.message
}

/** Display a text file or image as an overlay over the page */
async function viewEntry(entry: ZipEntry): Promise<void> {
  clearElement(viewerContentsElement)
  viewerNameElement.innerText = entry.filename

  const ext = findExt(entry.filename)
  if (entry.filename.endsWith('.txt')) {
    const contentsText = await entry.text()
    const textElement = document.createElement('pre')
    textElement.append(contentsText)
    viewerContentsElement.append(textElement)
  } else if (IMAGE_EXTENSIONS.includes(ext)) {
    const contentsUrl = URL.createObjectURL(await entry.blob())
    const imageElement = document.createElement('img')
    imageElement.src = contentsUrl
    viewerContentsElement.append(imageElement)
    currentObjectUrl = contentsUrl
  } else {
    viewerContentsElement.innerText = 'Cannot view this file type'
  }

  // Entry comment
  if (entry.comment) {
    viewerCommentElement.innerText = 'Comment: ' + entry.comment
  } else {
    clearElement(viewerCommentElement)
  }

  // Removing this class causes the element to fill the page as an overlay
  viewerElement.style.display = 'flex'
}

/** Save decompressed archive entry as a file download */
async function downloadEntry(entry: ZipEntry): Promise<void> {
  const entryUrl = URL.createObjectURL(await entry.blob())
  downloadElement.setAttribute('href', entryUrl)
  downloadElement.setAttribute('download', entry.filename)
  downloadElement.click()
  // Does not seem to be an issue to revoke this URL after calling click()
  URL.revokeObjectURL(entryUrl)
}

/** Renders a number of bytes as KB/MB/etc. */
function humanSize(size: number): string {
  let sizeFormatted: string
  let sizeUnit: string
  if (size < 1024) {
    sizeFormatted = size.toString()
    sizeUnit = 'B'
  } else if (size < 1024 ** 2) {
    sizeFormatted = (size / 1024).toLocaleString(undefined, {maximumFractionDigits: 2})
    sizeUnit = 'KB'
  } else if (size < 1024 ** 3) {
    sizeFormatted = (size / 1024 ** 2).toLocaleString(undefined, {maximumFractionDigits: 2})
    sizeUnit = 'MB'
  } else {
    sizeFormatted = (size / 1024** 3).toLocaleString(undefined, {maximumFractionDigits: 2})
    sizeUnit = 'GB'
  }
  return `${sizeFormatted}${sizeUnit}`
}

/** Generate a single row for the archive contents listing */
function buildZipListItem(entry: ZipEntry): HTMLLIElement {
  const listItemElement = document.createElement('li')

  const entryNameElement = document.createElement('span')
  const entrySizeElement = document.createElement('span')
  entrySizeElement.className = 'filesize'
  entrySizeElement.append(` (${humanSize(entry.size)})`)
  entryNameElement.append(entry.filename, entrySizeElement)
  listItemElement.append(entryNameElement)

  const listItemDownloadElement = document.createElement('a')
  listItemDownloadElement.append('Save file')
  listItemDownloadElement.addEventListener('click', (ev) => {
    ev.stopPropagation()
    downloadEntry(entry)
  })
  listItemElement.append(listItemDownloadElement)

  listItemElement.addEventListener('click', () => {viewEntry(entry)})

  return listItemElement
}

/** Opens an URL or Blob with Zipcord, or displays a message on errors */
async function openArchiveOrSetError(source: string | Blob): Promise<Zipcord> {
  clearElement(archiveCommentElement)
  if (typeof source === 'string') {
    archiveNameElement.innerText = `Loading ${source}...`
  } else {
    archiveNameElement.innerText = `Loading local file...`
  }

  try {
    const zip = await Zipcord.open(source)
    return zip
  } catch (e) {
    setErrorMessage(e)
    throw e
  }
}

/** Fill the zip listing with an archive's entries */
function populateZipListing(zip: Zipcord): void {
  const sourceName = typeof zip.originalSource === 'string' ? zip.originalSource : '<buffer>'
  archiveNameElement.innerText = `${sourceName} `
  const archiveSizeElement = document.createElement('span')
  archiveSizeElement.className = 'filesize'
  archiveSizeElement.append(`(${humanSize(zip.filesize)})`)
  archiveNameElement.append(archiveSizeElement)

  clearElement(archiveCommentElement)
  if (zip.comment) {
    const commentFormattedElement = document.createElement('i')
    commentFormattedElement.append(zip.comment)
    archiveCommentElement.append('Archive comment: ', commentFormattedElement)
  }

  clearElement(archiveListingElement)
  for (const entry of zip.entries.values()) {
    archiveListingElement.append(buildZipListItem(entry))
  }
}

/** Attach event handlers to UI */
function initPage(): void {
  fileUrlButtonElement.addEventListener('click', async () => {
    const url = fileUrlInputElement.value
    const zip = await openArchiveOrSetError(url)
    populateZipListing(zip)
  })

  fileBlobInputElement.addEventListener('change', async () => {
    const file = fileBlobInputElement.files?.[0]
    // file will be undefined if files is empty array
    if (file) {
      const zip = await openArchiveOrSetError(file)
      populateZipListing(zip)
    }
  })

  viewerElement.addEventListener('click', () => {
    viewerElement.style.display = 'none'
    URL.revokeObjectURL(currentObjectUrl)
  })
}

initPage()
