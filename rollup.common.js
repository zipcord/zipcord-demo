import babel from '@rollup/plugin-babel'
import commonjs from '@rollup/plugin-commonjs'
import copy from 'rollup-plugin-copy'
import nodeResolve from '@rollup/plugin-node-resolve'
import { terser } from 'rollup-plugin-terser'


const COMMON_FILES = [
  'src/example.zip', 'src/style.css',
  'node_modules/zicord-decoder-flate2/decoder-pkg/'
]
const extensions = ['.js', '.ts']

export function makeConfig(input, files, plugins = []) {
  return {
    input,
    output: {
      dir: 'public',
      format: 'iife',
      compact: true
    },
    plugins: [
      commonjs(),
      copy({
        targets: [
          {
            src: [...COMMON_FILES, ...files],
            dest: 'public'
          }
        ]
      }),
      nodeResolve({
        browser: true,
        extensions,
        customResolveOptions: {
          moduleDirectory: ['node_modules', 'src']
        }
      }),
      babel({
        babelHelpers: 'bundled',
        extensions,
        presets: [
          "@babel/typescript",
          "@babel/env"
        ]
      }),
      ...plugins,
      terser({
        compress: {
          passes: 2,
          unsafe: true
        }
      }),
    ]
  }
}
