module.exports = {
  "parser": "@typescript-eslint/parser",
  "plugins": [
    "@typescript-eslint"
  ],
  "extends": [
    "eslint:recommended",
    "plugin:eslint-comments/recommended",
    "plugin:@typescript-eslint/eslint-recommended",
    "plugin:@typescript-eslint/recommended"
  ],
  "rules": {
    "eqeqeq": "error",
    "key-spacing": "error",
    "no-console": "warn",
    'no-constant-condition': ['error', {checkLoops: false}],
    "no-import-assign": "error",
    "no-multiple-empty-lines": "error",
    "no-trailing-spaces": "error",
    "no-unneeded-ternary": "error",
    "no-var": "error",
    "prefer-const": "warn",
    "semi": [
      "error",
      "never",
      {
        "beforeStatementContinuationChars": "always"
      }
    ],
    "@typescript-eslint/explicit-function-return-type": [
      "warn",
      {
        "allowExpressions": true
      }
    ],
    "@typescript-eslint/member-delimiter-style": [
      "warn",
      {
        "multiline": {
          "delimiter": "none"
        }
      }
    ],
    "@typescript-eslint/no-empty-function": "off",
    // No need to check this, TS already does
    "@typescript-eslint/no-unused-vars": "off"
  }
}
