// TODO:NPM: Reenable flate2 decoder when published to NPM

import * as fs from 'fs'
import * as path from 'path'

import { ZipEntry, ZipcordRaw, nodeConfigSetup } from 'zipcord'
import { fflateDecoderSetup } from 'zipcord-decoder-fflate'
// import { createFlate2Setup } from 'zipcord-decoder-flate2'
import { pakoDecoderSetup } from 'zipcord-decoder-pako'


/* eslint-disable no-console */

const DEFAULT_ITERATIONS = 50
const ZIP_ENTRY_NAME = 'camels-uncompressed.png'
const ZIP_ENTRY_SIZE = 1401531
const ZIP_PATH = '../src/example.zip'


async function timeExtract(entry: ZipEntry, iterations: number): Promise<number> {
  const start = process.hrtime.bigint()
  for (let i = 0; i < iterations; i++) {
    await entry.buffer()
  }
  const end = process.hrtime.bigint()

  return Number(end - start)
}

async function runBenchmark(archives: Map<string, ZipcordRaw>, iterations: number): Promise<void> {
  const entries = Array.from(archives.entries())
  for (const [archiveName, archive] of entries.values()) {
    const entry = archive.entries.get(ZIP_ENTRY_NAME)
    if (!entry) {
      throw new Error('Archive entry not found')
    }
    const time = await timeExtract(entry, iterations) / 10**9
    const speed = (ZIP_ENTRY_SIZE / (time / iterations)) / 1024**2
    console.log(`${archiveName}: ${time.toFixed(2)}s (${speed.toFixed(2)} MB/s)`)
  }
}

async function loadData(): Promise<Map<string, ZipcordRaw>> {
  const archiveFile = fs.readFileSync(path.join(__dirname, ZIP_PATH))

  const zipFflate = await ZipcordRaw.open(archiveFile, {
    decoderSetup: fflateDecoderSetup
  })
  const zipFflateNoStreams = await ZipcordRaw.open(archiveFile, {
    decoderSetup: fflateDecoderSetup, disableInternalStreams: true
  })

  // const zipFlate2 = await ZipcordRaw.open(archiveFile, {
  //   decoderSetup: createFlate2Setup('decoder-pkg/index_bg.wasm')
  // })
  // const zipFlate2NoStreams = await ZipcordRaw.open(archiveFile, {
  //   decoderSetup: createFlate2Setup('decoder-pkg/index_bg.wasm'),
  //   disableInternalStreams: true
  // })
  // await zipFlate2.decoder

  const zipPako = await ZipcordRaw.open(archiveFile, {
    decoderSetup: pakoDecoderSetup
  })
  const zipPakoNoStreams = await ZipcordRaw.open(archiveFile, {
    decoderSetup: pakoDecoderSetup, disableInternalStreams: true
  })

  const zipNode = await ZipcordRaw.open(archiveFile, {
    decoderSetup: nodeConfigSetup
  })
  const zipNodeNoStreams = await ZipcordRaw.open(archiveFile, {
    decoderSetup: nodeConfigSetup, disableInternalStreams: true
  })

  const archives = new Map([
    ['fflate', zipFflate],
    ['fflate (no streams)', zipFflateNoStreams],
    // ['Flate2', zipFlate2],
    // ['Flate2 (no streams)', zipFlate2NoStreams],
    ['Pako', zipPako],
    ['Pako (no streams)', zipPakoNoStreams],
    ['Node.js native zlib', zipNode],
    ['Node.js native zlib (no streams)', zipNodeNoStreams],
  ])

  return archives
}

async function main(): Promise<void> {
  const archives = await loadData()
  const parsedIterations = parseInt(process.argv[2])
  const iterations = isNaN(parsedIterations) ? DEFAULT_ITERATIONS : parsedIterations
  await runBenchmark(archives, iterations)
}
main()

/* eslint-enable no-console */
