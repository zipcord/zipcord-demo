import { sizeSnapshot } from 'rollup-plugin-size-snapshot'

import { makeConfig } from './rollup.common'


export default makeConfig(
  'src/index.ts',
  ['src/index.html'],
  [ sizeSnapshot() ]
)
